# Beancount Vagrant (beancount-vagrant)

This micro-project aims to provide plugins and scripts which compliment the use of the Beancount CLI system.

Vagrant is a tool for scripting entire Virtual Machines, and this project uses this in order to create a VM for beancount and related software.

**Author**: Alex Ford (arf4188@gmail.com)

**License**: MIT License (see [LICENSE](LICENSE) file for more details)

## Dependencies

In addition to this Git repo, you will also need the following other repos:

* None

## Quick Start Usage

TBD

## Common Conventions

In all of my beancount-* repos, I follow the following conventions:

* Beancount data files (with your actual transactions in them) use the .bcdat extension
* Beancount spec files (which are supporting files used by my scripts and plugins) use the .bcspec extension